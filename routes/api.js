const express = require('express')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const mysql = require('mysql')
const router = express.Router()

const db = require('../db/index.js')

const saltRounds = 10

router.get('/test',function(req,res){
  //get_accounts(req, res, req.body.AccountId);
  var sql="select username from user";
  db.query(sql,(err,rows)=>{
    if(err){
      res.json({err:"unable to connect with mysql"})
    }
    else{
      res.json({list:rows})
    }
  })
});  

// const getCookie = (key)=>{
//   const cookies = req.headers.cookie;
//   const arr = cookies.split("; ");
//   for(var i=0;i<arr.length;i++){
//       let newArr = arr[i].split("=");
//       if(newArr[0] == key){
//           return newArr[1];
//       }
//   }
// }
router.get('/checkAuth', (req, res,next) => {
  // let token = getCookie("token");
  const token = req.cookies.token
  if (!token) {
    res.status(401).send('No token provided')
  } else {
    jwt.verify(token, process.env.JWT_ENCRYPTION, (err, decoded) => {
      if (err) {
        res.status(401).send('Unauthorized user')
      } else {
        res.send('Authenticated')
      }
    })
  }
})

router.get('/register', (req, res, next) => {
  const { username, password } = req.body
 
  bcrypt.hash(password, saltRounds, (err, hashed) => {
    if (err) {
      console.log("error!!!")
      next(err)
    }
    const values = [username, hashed]
    console.log(values);
    var sql = 'INSERT INTO user (username, password) VALUES (?,?)'
    db.query([values],sql,(err, res, fields) => {
      if (err) {
        res.status(409).send('Username already exists')
      } 
      else {
        res.sendStatus(200)

      }
    })
  })
})

router.post('/authenticate', (req, res) => {
  const { username, password } = req.body
  var sql = 'SELECT * FROM user WHERE username = ';
  db.query(sql, (err, result) => {
    if (err) {
      res.status(500).send(err.message)
    } else if (!result.length) {
      res.status(401).send('Incorrect username')
    } else {
      bcrypt.compare(password, result[0].password, (err, same) => {
        if (err) {
          res.status(500).send(err.message)
        } else if (!same) {
          res.status(401).send('Incorrect password')
        } else {
          // issue token
          const payload = { username }
          const token = jwt.sign(payload, process.env.JWT_ENCRYPTION, { expiresIn: process.env.JWT_EXPIRATION })
          res.cookie('token', token, { httpOnly: true }).sendStatus(200)
        }
      })
    }
  })
})

module.exports = router
